# Guia de Instalacion

Lo primero que haremos sera instalar los paquetes de node para el proyecto

```bash
npm install
```

Luego iniciaremos nuestro docker-compose para que levantar nuestra base de datos, para ello se recomienda hacer uso de linux

```bash
sudo docker-compose up -d database
```

Ahora vamos a generar nuestras tablas iniciales

```bash
npm run migration:generate --name=init
```

Luego vamos a correr nuestras tabla

```bash
npm run migration:run
```

Una vez realizado todo estos pasos lograremos hacer correr el backend

```bash
npm run star:dev
```

Para el frontend lo que necesitamos es ingresar a su respectiva carpeta e instalar sus propios modulos de node

```bash
cd frontend
npm install
```

ahora vamos a levantar nuestro frontend

```bash
npm run dev
```

una vez realizado estos pasos debemos ingresar al localhost:3001 donde nuestro fron se encontrara corriendo
