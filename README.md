# README

EL HOCICÓN es un portal web sobre noticias donde se planea encontrar sitios web

![image](https://i.postimg.cc/4xkFcwh6/imagen.png)

En este sitio web podras crear, actualizar y eliminar haciendo clic en noticias en la parte superior derecha

![image](https://i.postimg.cc/02zPsRz4/imagen.png)

Solo se debe llenar los campos requerido y luego enviar

![image](https://i.postimg.cc/j5hPtnFR/imagen.png)

Luego de enviar podra notar la modificacion en la pagina principal

![image](https://i.postimg.cc/vZdYL2mS/imagen.png)

Para eliminar un archivo debes acceder a Noticias>Modificar Noticia y notaras el siguiente menú

![image](https://i.postimg.cc/nrB5FJD4/imagen.png)

Para eliminar haremos clic en el boton que tiene una cruz, posterior a esta acción podremos comprobar su eliminacion, en este caso eliminaremos el primer elemento

![image](https://i.postimg.cc/Ssw7xY65/imagen.png)

Para hacer la modificacion de una noticia nos iremos a Noticias>Moficar Noticia y ahora haremos clic en el boton de editar

![image](https://i.postimg.cc/ZRR45Cbr/imagen.png)

Esta accion nos redirigira a un formulario donde se tendra la informacion definida anteriormente

![image](https://i.postimg.cc/MGY18rHn/imagen.png)

A continuacion se modificara el titulo, la fecha y la imagen

![image](https://i.postimg.cc/6QHLvmXD/imagen.png)

Una vez realizado los cambios le damos clic en eviar y luego podremos notar los cambios en la pagina principal

![image](https://i.postimg.cc/zvYLbCVs/imagen.png)

> Nota: Por falta de tiempo no se ha logrado establecer los seed por lo que el sitio web iniciara sin datos y para ello se necesita crear manualmente
