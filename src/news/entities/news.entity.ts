import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class News {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column({ nullable: true })
  image: string;

  @Column()
  date: Date;

  @Column()
  place: string;

  @Column()
  author: string;

  @Column()
  description: string;
}
