// seed.service.ts
import { Injectable } from '@nestjs/common';
import { NewsService } from './services/news.service';

@Injectable()
export class SeedService {
  constructor(private readonly newsService: NewsService) {}

  async seed() {
    const newsData = [
      // Agrega aquí los datos iniciales que desees
      {
        title: 'Noticia 1',
        image: 'imagen_1.jpg',
        date: new Date(),
        place: 'Lugar 1',
        author: 'Autor 1',
        description: 'Descripción 1',
      },
      {
        title: 'Noticia 2',
        image: 'imagen_2.jpg',
        date: new Date(),
        place: 'Lugar 2',
        author: 'Autor 2',
        description: 'Descripción 2',
      },
      // ... puedes agregar más datos según sea necesario
    ];

    for (const data of newsData) {
      await this.newsService.create(data);
    }

    console.log('Seed completed.');
  }
}
