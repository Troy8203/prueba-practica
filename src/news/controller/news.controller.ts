import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Put,
  Delete,
} from '@nestjs/common';

import { NewsService } from './../services/news.service';

@Controller('api/news')
export class NewsController {
  constructor(private newsService: NewsService) {}

  @Get()
  getAllNews() {
    return this.newsService.findAll();
  }

  @Get(':id')
  getOneNews(@Param('id') id: number) {
    return this.newsService.findOne(id);
  }

  @Post()
  createNews(@Body() body: any) {
    return this.newsService.create(body);
  }

  @Put(':id')
  updateNews(@Param('id') id: number, @Body() body: any) {
    return this.newsService.update(id, body);
  }

  @Delete(':id')
  deleteNews(@Param('id') id: number) {
    return this.newsService.delete(id);
  }
}
