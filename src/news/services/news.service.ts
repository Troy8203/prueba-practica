import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { News } from '../entities/news.entity';

@Injectable()
export class NewsService {
  constructor(@InjectRepository(News) private newsRepo: Repository<News>) {}

  findAll() {
    return this.newsRepo.find();
  }

  findOne(id: number) {
    return this.newsRepo.findOne({ where: { id } });
  }

  create(body: any) {
    const newNews = this.newsRepo.create(body);
    return this.newsRepo.save(newNews);
  }

  async update(id: number, body: any) {
    const news = await this.newsRepo.findOne({ where: { id } });
    this.newsRepo.merge(news, body);
    return this.newsRepo.save(news);
  }

  async delete(id: number) {
    await this.newsRepo.delete(id);
    return true;
  }
}
