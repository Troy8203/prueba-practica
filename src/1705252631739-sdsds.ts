import { MigrationInterface, QueryRunner } from "typeorm";

export class Sdsds1705252631739 implements MigrationInterface {
    name = 'Sdsds1705252631739'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "news" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "image" character varying NOT NULL, "date" TIMESTAMP NOT NULL, "place" character varying NOT NULL, "author" character varying NOT NULL, "description" character varying NOT NULL, CONSTRAINT "PK_39a43dfcb6007180f04aff2357e" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "news"`);
    }

}
