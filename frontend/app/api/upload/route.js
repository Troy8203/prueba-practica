import { writeFile } from 'fs/promises';
import path from 'path';

export async function POST(request) {
  const data = await request.formData();
  console.log(data);
  const file = data.get('file');
  console.log(file);

  const bytes = await file.arrayBuffer();
  const buffer = Buffer.from(bytes);

  const filePath = path.join(process.cwd(), 'public', file.name);
  writeFile(filePath, buffer);

  return new Response(
    JSON.stringify({
      message: 'uploaded file',
    }),
  );
}
