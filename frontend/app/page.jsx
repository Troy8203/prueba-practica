'use client';
import Button from '@mui/material/Button';
import Box from '@mui/system/Box';
import TextField from '@mui/material/TextField';

import SearchIcon from '@mui/icons-material/Search';
import TuneIcon from '@mui/icons-material/Tune';

import Grid from '@mui/material/Grid';

import CardNews from '../components/CardNews';

import React, { useState, useEffect } from 'react';

function Media(props) {
  const [news, setNews] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch('http://localhost:3000/api/news');
        const data = await response.json();
        setNews(data);
      } catch (error) {
        console.error('Error fetching news:', error);
      }
    };

    fetchData();
  }, []);

  return (
    <Grid container wrap="wrap" sx={{ justifyContent: 'space-evenly' }}>
      {news.map((item, index) => (
        <CardNews key={index} item={item} index={index} />
      ))}
    </Grid>
  );
}

export default function Home() {
  return (
    <>
      <Box
        component="section"
        sx={{
          my: 2,
          px: 1,
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'center',
          gap: 1,
        }}
      >
        <Button
          sx={{
            mr: 2,
            display: 'flex',
            flexDirection: 'row-reverse',
            justifyContent: 'center',
            gap: 1,
          }}
        >
          <TuneIcon />
          Filtro
        </Button>
        <TextField
          id="outlined-basic"
          label="Buscar"
          variant="outlined"
          sx={{ flexGrow: 1 }}
        />
        <Button
          sx={{
            mr: 2,
            display: 'flex',
            flexDirection: 'row-reverse',
            justifyContent: 'center',
            gap: 1,
          }}
        >
          <SearchIcon />
          Buscar
        </Button>
      </Box>
      <Box sx={{ overflow: 'hidden' }}>
        <Media />
      </Box>
    </>
  );
}
