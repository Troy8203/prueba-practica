'use client';
import Box from '@mui/system/Box';
import Link from '@mui/material/Link';
import AddToPhotosIcon from '@mui/icons-material/AddToPhotos';
import SyncIcon from '@mui/icons-material/Sync';
import EditIcon from '@mui/icons-material/Edit';

function handleClick(event) {
  event.preventDefault();
  console.info('You clicked a breadcrumb.');
}

export default function RootLayout({ children }) {
  return (
    <>
      <div role="presentation">
        <Box
          aria-label="breadcrumb"
          sx={{
            color: '#1976d2',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            gap: 2,
          }}
        >
          <Link
            underline="hover"
            sx={{ display: 'flex', alignItems: 'center' }}
            color="inherit"
            href="/news/create"
          >
            <AddToPhotosIcon sx={{ mr: 0.5 }} fontSize="inherit" />
            Nueva Noticia
          </Link>
          |
          <Link
            underline="hover"
            sx={{ display: 'flex', alignItems: 'center' }}
            color="inherit"
            href="/news/modify"
          >
            <EditIcon sx={{ mr: 0.5 }} fontSize="inherit" />
            Modificar Noticia
          </Link>
        </Box>
      </div>
      <Box component="div" sx={{ px: 2, py: 1, m: 0 }}>
        {children}
      </Box>
    </>
  );
}
