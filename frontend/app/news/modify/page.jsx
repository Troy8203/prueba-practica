'use client';
import React, { useState, useEffect } from 'react';
import Grid from '@mui/material/Grid';

import CardsModify from '../../../components/CardsModify';

export default function Modify() {
  const [news, setNews] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch('http://localhost:3000/api/news');
        const data = await response.json();
        setNews(data);
      } catch (error) {
        console.error('Error fetching news:', error);
      }
    };

    fetchData();
  }, []);

  return (
    <Grid container wrap="wrap" sx={{ justifyContent: 'space-evenly' }}>
      {news.map((item, index) => (
        <CardsModify key={index} item={item} index={index} />
      ))}
    </Grid>
  );
}
