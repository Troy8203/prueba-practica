'use client';
import React, { useState, useEffect } from 'react';
import { useParams } from 'next/navigation';
import { TextField, FormControl, Button } from '@mui/material';
import CardsModify from '../../../../components/CardNews';

export default function Edit() {
  const params = useParams();
  const idParams = params.id;

  const [title, setTitle] = useState('');
  const [image, setImage] = useState('');
  const [date, setDate] = useState('');
  const [place, setPlace] = useState('');
  const [author, setAuthor] = useState('');
  const [description, setDescription] = useState('');

  const [file, setFile] = useState(null);

  const [news, setNews] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(
          `http://localhost:3000/api/news/${idParams}`,
        );
        const data = await response.json();
        setNews(data);

        setTitle(data.title);
        setImage(data.image);
        setDate(data.date);
        setPlace(data.place);
        setAuthor(data.author);
        setDescription(data.description);
      } catch (error) {
        console.error('Error fetching news:', error);
      }
    };

    fetchData();
  }, []);

  return (
    <>
      <div>
        <form
          onSubmit={async (e) => {
            e.preventDefault();
            if (!file) return;

            const form = new FormData();
            form.set('file', file);
            //send to server
            const res = await fetch('/api/upload', {
              method: 'POST',
              body: form,
            });
            const data = res.json();
            console.log(data);

            const formData = {
              title,
              image,
              date,
              place,
              author,
              description,
            };

            const resApi = await fetch(
              `http://localhost:3000/api/news/${idParams}`,
              {
                method: 'PUT',
                headers: {
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify(formData),
              },
            );
            console.log(resApi);
          }}
        >
          <TextField
            label="Titulo"
            onChange={(e) => setTitle(e.target.value)}
            required
            variant="outlined"
            color="secondary"
            type="text"
            sx={{ mb: 3 }}
            fullWidth
            value={title}
          />
          <TextField
            label="Lugar"
            onChange={(e) => setPlace(e.target.value)}
            required
            variant="outlined"
            color="secondary"
            type="text"
            sx={{ mb: 3 }}
            fullWidth
            value={place}
          />
          <TextField
            label="Autor"
            onChange={(e) => setAuthor(e.target.value)}
            required
            variant="outlined"
            color="secondary"
            type="text"
            sx={{ mb: 3 }}
            fullWidth
            value={author}
          />
          <TextField
            onChange={(e) => setDate(e.target.value)}
            required
            variant="outlined"
            color="secondary"
            type="date"
            sx={{ mb: 3 }}
            fullWidth
            value={date}
          />
          <TextField
            label="Contenido"
            onChange={(e) => setDescription(e.target.value)}
            required
            variant="outlined"
            color="secondary"
            type="text"
            multiline
            rows={4}
            sx={{ mb: 3 }}
            fullWidth
            value={description}
          />

          <Button variant="contained" component="label">
            Cargar archivo...
            <input
              hidden
              type="file"
              onChange={(e) => {
                setFile(e.target.files[0]);
                setImage(e.target.files[0].name);
              }}
            />
          </Button>
          <Button type="submit">Enviar</Button>
        </form>
      </div>
    </>
  );
}
