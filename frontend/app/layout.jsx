import Navbar from '../components/Navbar.jsx';
import Box from '@mui/system/Box';

export const metadata = {
  title: 'Home',
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <Box component="body" sx={{ p: 0, m: 0 }}>
        <Navbar />
        <Box component="main" sx={{ px: 2, py: 1, m: 0 }}>
          {children}
        </Box>
      </Box>
    </html>
  );
}
