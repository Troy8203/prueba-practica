import Typography from '@mui/material/Typography';
import Skeleton from '@mui/material/Skeleton';
import Box from '@mui/system/Box';

function CardNews({ item, index }) {
  return (
    <Box
      key={index}
      sx={{
        background: '#e7e7e7',
        p: 2,
        borderRadius: 3,
        width: 210,
        marginRight: 0.5,
        my: 5,
      }}
    >
      {item ? (
        <img
          style={{ width: 210, height: 118 }}
          alt={item.title}
          src={item.image}
        />
      ) : (
        <Skeleton variant="rectangular" width={210} height={118} />
      )}

      {item ? (
        <Box sx={{ pr: 2 }}>
          <Typography gutterBottom variant="body2">
            {item.title}
          </Typography>
          <Typography display="block" variant="caption" color="text.secondary">
            {item.description.substring(0, 150) + ' ...'}
          </Typography>
          <Typography variant="caption" color="text.secondary">
            {`${item.author} • ${new Date(item.date).getUTCDate()}/${new Date(item.date).getUTCMonth() + 1}/${new Date(item.date).getUTCFullYear()}`}
          </Typography>
        </Box>
      ) : (
        <Box sx={{ pt: 0.5 }}>
          <Skeleton />
          <Skeleton width="60%" />
        </Box>
      )}
    </Box>
  );
}

export default CardNews;
