import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import NewspaperIcon from '@mui/icons-material/Newspaper';
import Link from '@mui/material/Link';

export default function Navbar() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            <Link href="/" underline="hover" sx={{ color: 'white' }}>
              EL HOCICÓN
            </Link>
          </Typography>
          <Link
            href="/news"
            underline="hover"
            sx={{
              color: 'white',
              display: 'flex',
              flexDirection: 'row-reverse',
              justifyContent: 'center',
              gap: 1,
            }}
          >
            Noticias
            <NewspaperIcon />
          </Link>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
