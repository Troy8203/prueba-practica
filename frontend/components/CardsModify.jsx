'use client';
import Typography from '@mui/material/Typography';
import Skeleton from '@mui/material/Skeleton';
import Box from '@mui/system/Box';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import EditIcon from '@mui/icons-material/Edit';
import CancelIcon from '@mui/icons-material/Cancel';
import Link from '@mui/material/Link';

import { useRouter } from 'next/navigation';

function CardNews({ item, index }) {
  const router = useRouter();

  const handleClickDelete = (id) => {
    const fetchData = async () => {
      try {
        const response = await fetch(`http://localhost:3000/api/news/${id}`, {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json',
          },
        });
        const data = await response.json();
        console.log(data);
        //refresh page
        router.push('/news/modify');
        router.refresh();
      } catch (error) {
        console.error('Error fetching news:', error);
      }
    };

    fetchData();
  };

  return (
    <Box
      key={index}
      sx={{
        background: '#e7e7e7',
        p: 2,
        borderRadius: 3,
        width: 210,
        marginRight: 0.5,
        my: 5,
      }}
    >
      {item ? (
        <img
          style={{ width: 210, height: 118 }}
          alt={item.title}
          src={'http://localhost:3001/' + item.image}
        />
      ) : (
        <Skeleton variant="rectangular" width={210} height={118} />
      )}

      {item ? (
        <Box sx={{ pr: 2 }}>
          <Typography gutterBottom variant="body2">
            {item.title}
          </Typography>
          <Typography display="block" variant="caption" color="text.secondary">
            {item.description.substring(0, 150) + ' ...'}
          </Typography>
          <Typography variant="caption" color="text.secondary">
            {`${item.author} • ${new Date(item.date).getUTCDate()}/${new Date(item.date).getUTCMonth() + 1}/${new Date(item.date).getUTCFullYear()}`}
          </Typography>
        </Box>
      ) : (
        <Box sx={{ pt: 0.5 }}>
          <Skeleton />
          <Skeleton width="60%" />
        </Box>
      )}
      <Stack
        spacing={2}
        direction="row"
        sx={{ mt: 2, justifyContent: 'flex-end' }}
      >
        <Button variant="contained" color="success">
          <Link href={`/news/modify/${item.id}`}>
            <EditIcon sx={{ color: 'white' }} />
          </Link>
        </Button>
        <Button
          variant="contained"
          color="error"
          type="button"
          onClick={() => handleClickDelete(item.id)}
        >
          <CancelIcon />
        </Button>
      </Stack>
    </Box>
  );
}

export default CardNews;
